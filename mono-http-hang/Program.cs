﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace mono_http_hang
{
    /* Run the test script
     * node echo.js
     * first so that this test has something to talk to
     * */
    class Program
    {
        static void Main(string[] args)
        {
            var client = new HttpClient();
            var tasks = new List<Task>();
            for (var i = 0; i < 10; ++i)
            {
                tasks.Add(Post100(client));
            }
            Task.WaitAll(tasks.ToArray(), new CancellationTokenSource(TimeSpan.FromSeconds(15)).Token);
        }

        private static int n = 0;
        private static Task Post100(HttpClient client)
        {
            return Task.Run(async () =>
            {
                Console.WriteLine("start");
                for (int k = 0; k < 100; ++k)
                {
                    var content = new StringContent("yolo");
                    using (var resp = await client.PostAsync("http://127.0.0.1:12345/foo", content))
                    {
                        var d = await resp.Content.ReadAsStringAsync();
                        if (d != "yolo")
                            Console.WriteLine("oh shitsky!");
                    }
                }

                Console.WriteLine("stop {0}", ++n);
            });
        }
    }
}
